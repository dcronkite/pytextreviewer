"""

"""
from util.decorator import IgnoreError


class ConceptCask(object):
    COMPLETED_COLUMN = 'completed'

    def __init__(self, header, data, eqdict):
        """
        Parameters
        ----------
        header = first row, header
        data = list of rows, each row is an entry/concept
        eqdict = dictionary from config file of
                standard name -> name in header

        Standard Names
        --------------
        concept - target text
        context - context of concept
        text - larger portion of text
        """
        self.header_ = header
        self.data_ = data
        self.userdata_ = None  # [{column: val, column2: val, ...} for all rows]
        self.ptr_ = 0  # current row of data
        self.text_ = []
        for el in eqdict['text']:
            self.text_.append(header.index(el))
        try:
            self.context_ = header.index(eqdict['context'])
        except ValueError:
            self.context_ = None
        try:
            self.concept_ = header.index(eqdict['concept'])
        except ValueError:
            self.concept_ = None
        try:
            self.patient_id_ = header.index(eqdict['patient-id'])
        except ValueError:
            self.patient_id_ = None
        try:
            self.report_number_ = header.index(eqdict['record-number'])
        except ValueError:
            self.report_number_ = None
        try:
            self.report_total_ = header.index(eqdict['record-total'])
        except ValueError:
            self.report_total_ = None

        self.extra_headers_ = eqdict['extra-headers']
        self.extra_text_ = [header.index(x) for x in eqdict['extra-headers']]

    def get_row(self):
        return self.ptr_

    def set_row(self, num):
        self.ptr_ = num

    def get_header(self):
        return self.header_

    def change_user(self, userdata):
        self.userdata_ = userdata

    @IgnoreError(errors=[TypeError], errorreturn='')
    def get_concept(self):
        return self.data_[self.ptr_][self.concept_]

    @IgnoreError(errors=[TypeError], errorreturn='')
    def get_context(self):
        return self.data_[self.ptr_][self.context_]

    def get_text(self):
        res = []
        for el in self.text_:
            res.append(self.data_[self.ptr_][el])
        return '||||'.join(res)

    @IgnoreError(errors=[TypeError], errorreturn='')
    def get_patient_id(self):
        return self.data_[self.ptr_][self.patient_id_]

    @IgnoreError(errors=[TypeError], errorreturn='')
    def get_record_number(self):
        return self.data_[self.ptr_][self.report_number_]

    @IgnoreError(errors=[TypeError], errorreturn='')
    def get_record_total(self):
        return self.data_[self.ptr_][self.report_total_]

    def get_current_row(self):
        return self.userdata_[self.ptr_]

    def add_input(self, col, inp):
        self.userdata_[self.ptr_][col] = inp

    def has_next(self):
        if self.ptr_ + 1 >= len(self.data_):
            return False
        return True

    def next(self):
        if self.has_next():
            self.ptr_ += 1
        else:
            self.ptr_ = 0

    def has_next_unanswered(self):
        for i in range(self.ptr_ + 1, len(self.data_)):
            if not self.userdata_[i][ConceptCask.COMPLETED_COLUMN]:
                return i
        for i in range(0, self.ptr_):
            if not self.userdata_[i][ConceptCask.COMPLETED_COLUMN]:
                return i
        return -1

    def next_unanswered(self):
        res = self.has_next_unanswered()
        if res == -1:
            return False
        else:
            self.ptr_ = res
            return True

    def has_prev(self):
        return self.ptr_ >= 0

    def prev(self):
        if self.has_prev():
            self.ptr_ -= 1
        else:
            self.ptr_ = len(self.userdata_) - 1

    def has_prev_unanswered(self):
        for i in range(self.ptr_ - 1, -1, -1):
            if not self.userdata_[i][ConceptCask.COMPLETED_COLUMN]:
                return i
        for i in range(len(self.data_) - 1, self.ptr_, -1):
            if not self.userdata_[i][ConceptCask.COMPLETED_COLUMN]:
                return i
        return -1

    def prev_unanswered(self):
        res = self.has_prev_unanswered()
        if res == -1:
            return False
        else:
            self.ptr_ = res
            return True

    def __len__(self):
        return len(self.data_)

    def get_line_as_string(self, index, delimiter='\t'):
        if index > len(self.data_):
            raise ValueError('Index out of bounds: Index: %d, Length: %d.' % (index, len(self.data_)))
        return delimiter.join(self.data_[index])

    def get_line_as_list(self, index):
        if index > len(self.data_):
            raise ValueError('Index out of bounds: Index: %d, Length: %d.' % (index, len(self.data_)))
        return self.data_[index]

    def progress(self):
        count = [0, 0]
        for row in range(self.__len__()):
            if self.userdata_[row][self.COMPLETED_COLUMN]:
                count[0] += 1
            else:
                count[1] += 1
        return tuple(count)

    def mark_done(self, completed=True):
        if completed:
            self.userdata_[self.ptr_][self.COMPLETED_COLUMN] = 'Done'
        else:
            self.userdata_[self.ptr_][self.COMPLETED_COLUMN] = ''  # empty marks not done

    def get_extra_text(self, idx):
        try:
            return self.data_[self.ptr_][self.extra_text_[idx]]
        except IndexError:
            return ''

    def get_extra_header(self, idx):
        try:
            return self.extra_headers_[idx]
        except IndexError:
            return 'Other'


class Categories(object):
    """
    Holds configuration information for categories.
    """

    def __init__(self, lst):
        """
        list of (column_name, visible name, button type)
        """
        self.cat_button = {}
        self.column_to_label_ = {}
        self.label_to_column_ = {}
        self.count_ = len(lst) + 1  # add one for COMPLETED_COLUMN
        self.cats_ = self.unpack_categories(lst)

    def __len__(self):
        return self.count_

    def unpack_categories(self, lst):
        cats = []
        for column, label, button_type in lst:
            self.label_to_column_[label] = column
            self.cat_button[label] = button_type
            cats.append(column)
            self.column_to_label_[column] = label
        return cats

    def get_category_button(self, cat):
        return self.cat_button[cat]

    def get_column_from_label(self, label):
        return self.label_to_column_[label]

    def get_label_from_columns(self, column):
        return self.column_to_label_[column]

    def get_categories(self):
        return self.cats_

    def update_categories(self, lst):
        self.cats_ = self.unpack_categories(lst)

    def get_all(self):
        return self.cats_

    def columns(self):
        return list(self.column_to_label_.keys()) + [ConceptCask.COMPLETED_COLUMN]

    def labels(self):
        return list(self.label_to_column_.keys()) + [ConceptCask.COMPLETED_COLUMN]

    def add_category(self, cat):
        self.cats_.append(cat)

    def get_column_label_pairs(self):
        return self.column_to_label_.items()
