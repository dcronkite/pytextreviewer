"""
Main gui

"""

import difflib
import string

import math
import wx
import wx.richtext
from collections import defaultdict

import ico
from controller import Controller


def similarEntry(w, lst):
    w = w.translate(string.maketrans('', ''), string.punctuation).lower()
    if len(w) > 3:
        return difflib.get_close_matches(w, lst, n=1, cutoff=0.8)
    else:
        return False


class HighlightRichTextCtrl(wx.richtext.RichTextCtrl):
    def __init__(self, parent, **kwargs):
        super(HighlightRichTextCtrl, self).__init__(parent, **kwargs)
        self.SetBackgroundColour('#DCDCDC')
        self.normal_font = wx.Font(11,
                                   wx.FONTFAMILY_SWISS,
                                   wx.FONTSTYLE_NORMAL,
                                   wx.FONTWEIGHT_NORMAL)
        self.highlight_font = wx.Font(11,
                                      wx.FONTFAMILY_SWISS,
                                      wx.FONTSTYLE_NORMAL,
                                      wx.FONTWEIGHT_BOLD)

    def SetValueAndHighlight(self, text, concept):
        """
        lst: list of terms to search for in text
        """
        self.SetValue('')
        concepts = [c.translate(string.maketrans('', ''), string.punctuation).lower() for c in concept.split() if
                    len(c) > 3]
        for num, section in enumerate(text.split('||||')):
            if num > 0:
                self.WriteText('\n')
            # self.WriteText('\n')
            for word in section.split():
                if len(word) > 3:
                    similar = similarEntry(word, concepts)
                else:
                    similar = False
                if similar:
                    self.BeginFont(self.highlight_font)
                    self.BeginTextColour('#A52A2A')
                else:
                    self.BeginFont(self.normal_font)
                    self.BeginTextColour('black')
                self.WriteText(word)
                self.WriteText(' ')
                self.EndTextColour()
                self.EndFont()

    def find(self, lst):
        """
        lst: list of terms to search for in text
        """
        self.SetSelectionRange((0, 10))
        self.ApplyBoldToSelection()


class ConfigureCategoriesDialog(wx.Dialog):
    def __init__(self, parent, configs, **kwargs):
        super(ConfigureCategoriesDialog, self).__init__(parent, -1, 'Configure Categories', **kwargs)

        self.SetScrollbar(orientation=wx.VERTICAL, position=1, range=100, thumbSize=200)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add((-1, 20))

        self.buttons = []
        hbox = wx.BoxSizer(wx.HORIZONTAL)

        hbox1 = wx.BoxSizer(wx.VERTICAL)
        hbox2 = wx.BoxSizer(wx.VERTICAL)
        hbox3 = wx.BoxSizer(wx.VERTICAL)

        for desc, abbr in configs:
            textarea = wx.TextCtrl(self, value=str(abbr))
            hbox1.Add(textarea)
            textlabel = wx.TextCtrl(self, value=desc)
            hbox2.Add(textlabel)
            removeBtn = wx.Button(self, -1, 'Remove')
            hbox3.Add(removeBtn)

            self.buttons.append((textlabel, textarea))

            hbox1.Add((-1, 15))
            hbox2.Add((-1, 15))
            hbox3.Add((-1, 13))

        hbox.AddSpacer((20, -1))
        hbox.Add(hbox1, proportion=2, flag=wx.EXPAND | wx.CENTER)
        hbox.Add(hbox2, proportion=2, flag=wx.EXPAND)
        hbox.Add(hbox3, proportion=1, flag=wx.ALIGN_RIGHT | wx.CENTER)
        vbox.Add(hbox, wx.EXPAND)

        hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        okBtn = wx.Button(self, wx.ID_OK, "OK")
        okBtn.SetDefault()
        hbox4.Add(okBtn, flag=wx.ALIGN_CENTER)
        cancelBtn = wx.Button(self, wx.ID_CANCEL, "Cancel")
        hbox4.Add(cancelBtn, flag=wx.ALIGN_CENTER)
        vbox.Add(hbox4, flag=wx.ALIGN_CENTER)

        self.SetSizerAndFit(vbox)

    def getCategories(self):
        res = []
        for desc, abbr in self.buttons:
            res.append((abbr.GetValue(), desc.GetValue()))
        return res


class ConfigureInit(wx.Dialog):
    def __init__(self, parent, configs, **kw):
        super(ConfigureInit, self).__init__(parent, -1, **kw)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add((-1, 20))

        self.buttons = []
        hbox = wx.BoxSizer(wx.HORIZONTAL)

        hbox1 = wx.BoxSizer(wx.VERTICAL)
        hbox2 = wx.BoxSizer(wx.VERTICAL)

        for label in configs:
            param = configs[label]

            #             hbox1 = wx.BoxSizer(wx.HORIZONTAL)
            textlabel = wx.StaticText(self, label=label)
            hbox1.Add(textlabel)
            textarea = wx.TextCtrl(self, value=str(param))
            hbox2.Add(textarea)
            #             vbox.Add(hbox1, flag=wx.CENTER)

            self.buttons.append((textlabel, textarea))

            #             vbox.Add((-1,15))
            hbox1.Add((-1, 15))
            hbox2.Add((-1, 15))

        hbox.AddSpacer((20, -1))
        hbox.Add(hbox1, proportion=2, flag=wx.EXPAND | wx.CENTER)
        hbox.Add(hbox2, proportion=1, flag=wx.ALIGN_RIGHT)
        vbox.Add(hbox, wx.EXPAND)

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        okBtn = wx.Button(self, wx.ID_OK, "OK")
        okBtn.SetDefault()
        hbox3.Add(okBtn, flag=wx.ALIGN_CENTER)
        cancelBtn = wx.Button(self, wx.ID_CANCEL, "Cancel")
        hbox3.Add(cancelBtn, flag=wx.ALIGN_CENTER)
        vbox.Add(hbox3, flag=wx.ALIGN_CENTER)

        self.SetSizer(vbox)
        self.SetSize((400, 400))

    def getUpdatedConfigs(self):
        resdict = {}
        for labelBtn, textBtn in self.buttons:
            resdict[labelBtn.GetLabel()] = textBtn.GetValue()
        return resdict


class TipButton(wx.Button):
    """Subclass of wx.Button, has the same functionality. 
    Allows user to set Status Bar help by using SetStatusText method.
    from: http://stackoverflow.com/questions/14916262/wxpython-description-of-event-in-status-bar
    """
    current = None

    def __init__(self, *args, **kwargs):
        wx.Button.__init__(self, *args, **kwargs)

    def SetStatusText(self, help_string, status_bar_frame):
        self.help_string = help_string
        self.status_bar_frame = status_bar_frame
        self.Bind(wx.EVT_ENTER_WINDOW, self._ShowHelp)
        self.Bind(wx.EVT_LEAVE_WINDOW, self._HideHelp)

    def _ShowHelp(self, e):
        TipButton.current = self.GetLabel()
        self.status_bar_frame.SetStatusText(self.help_string)

    def _HideHelp(self, e):
        if self.GetLabel() == TipButton.current:
            self.status_bar_frame.SetStatusText("")

            #


# class ModalQuestion(wx.Dialog):
#     """ Ask a question.
# 
#     Modal return value will be the index into the list of buttons.  Buttons can be specified
#     either as strings or as IDs.
#     """
# 
#     def __init__(self, parent, message, buttons, **kw):
#         wx.Dialog.__init__(self, parent, **kw)
# 
#         topSizer = wx.BoxSizer(orient=wx.VERTICAL)
#         self.SetSizer(topSizer)
# 
#         topSizer.Add(wx.StaticText(self, label=message), flag=wx.ALIGN_CENTRE|wx.ALL, border=5)
# 
#         line = wx.StaticLine(self, size=(20, -1), style=wx.LI_HORIZONTAL)
#         topSizer.Add(line, flag=wx.EXPAND|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, border=5)
# 
#         buttonSizer = wx.BoxSizer(orient=wx.HORIZONTAL)
#         topSizer.Add(buttonSizer, flag=wx.ALIGN_CENTRE)
# 
#         for i, button in enumerate(buttons):
#             if isinstance(button, (int, long)):
#                 b = wx.Button(self, id=button)
#             else:
#                 b = wx.Button(self, label=button)
# 
#             eventManager.Register(dropArgs(curry(self.EndModal, i)), wx.EVT_BUTTON, b)
#             buttonSizer.Add(b, flag=wx.ALL, border=5)
# 
#         self.Fit()
# 
# def questionDialog(message, buttons=[wx.ID_OK, wx.ID_CANCEL], caption=''):
#     """ Ask a question.
# 
#     Return value will be the button the user clicked, in whatever form it was specified.
#     Allowable button specifications are strings or wxIDs of stock buttons.
# 
#     If the user clicks the 'x' close button in the corner, the return value will be None.
#     """
# 
#     dlg = ModalQuestion(None, message, buttons, title=caption)
#     try:
#         return buttons[dlg.ShowModal()]
#     except IndexError:
#         return None
# 
# class curry(object):
#     """Taken from the Python Cookbook, this class provides an easy way to
#     tie up a function with some default parameters and call it later.
#     See http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/52549 for more.
#     """
#     def __init__(self, func, *args, **kwargs):
#         self.func = func
#         self.pending = args[:]
#         self.kwargs = kwargs
# 
#     def __call__(self, *args, **kwargs):
#         if kwargs and self.kwargs:
#             kw = self.kwargs.copy()
#             kw.update(kwargs)
#         else:
#             kw = kwargs or self.kwargs
#         return self.func(*(self.pending + args), **kw)
# 
# class dropArgs(object):
#     """ Same as curry, but once the function is built, further args are ignored. """
# 
#     def __init__(self, func, *args, **kwargs):
#         self.func = func
#         self.args = args[:]
#         self.kwargs = kwargs
# 
#     def __call__(self, *args, **kwargs):
#         return self.func(*self.args, **self.kwargs)


class TwoTextEntryDialog(wx.Dialog):
    def __init__(self, parent, textList):
        super(TwoTextEntryDialog, self).__init__(parent, -1, 'Add Description')

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add((-1, 20))

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        longLabel = wx.StaticText(self, label='Description:')
        hbox1.Add(longLabel)
        self.longArea = wx.TextCtrl(self)
        hbox1.Add(self.longArea)
        vbox.Add(hbox1, flag=wx.CENTER)

        vbox.Add((-1, 15))

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        shortLabel = wx.StaticText(self, label='Button (Radio/Text):')
        hbox2.Add(shortLabel, flag=wx.CENTER)
        self.shortArea = wx.TextCtrl(self)
        hbox2.Add(self.shortArea, flag=wx.CENTER)
        vbox.Add(hbox2, flag=wx.CENTER)

        vbox.Add((-1, 15))

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        okBtn = wx.Button(self, wx.ID_OK, "OK")
        okBtn.SetDefault()
        hbox3.Add(okBtn, flag=wx.ALIGN_CENTER)
        cancelBtn = wx.Button(self, wx.ID_CANCEL, "Cancel")
        hbox3.Add(cancelBtn, flag=wx.ALIGN_CENTER)
        vbox.Add(hbox3, flag=wx.ALIGN_CENTER)

        self.SetSizer(vbox)
        self.Centre()

    def getDescription(self):
        return self.longArea.GetValue()

    def getAbbreviation(self):
        return self.shortArea.GetValue()


class ConceptPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.label = wx.StaticText(self, label='Concept: ')
        self.textArea = wx.TextCtrl(self, value='This is the concept.')
        #
        self.sizer.Add(self.label, flag=wx.RIGHT, border=8)
        self.sizer.Add(self.textArea, proportion=1)

    def updateText(self, text):
        self.textArea.ChangeValue(text)


class MainWindow(wx.Frame):
    def __init__(self, parent, title, **configs):
        super(MainWindow, self).__init__(parent, title=title, size=(600, 600))
        self.label_to_buttons = defaultdict(list)  # label-> [button, ]
        self.SetIcon(ico.textrev.GetIcon())
        self.controller = Controller(**configs)
        self.categories = self.controller.get_categories()
        self.InitUI()
        self.Centre()
        self.Show()
        self.UpdateButtonLayout()
        self.GetUser()
        self.PopulateFields()
        self.EnableButtons()

    def InitUI(self):

        # filemenu
        filemenu = wx.Menu()
        menuSave = filemenu.Append(wx.ID_SAVE, '&Save', help='Save progress.')
        menuSaveExit = filemenu.Append(wx.ID_SAVE, 'Save and E&xit', help='Save progress and terminate program.')
        filemenu.AppendSeparator()
        exportFile = filemenu.Append(wx.ID_APPLY, 'Export File to CSV', help='Export results to a csv file.')
        filemenu.AppendSeparator()
        menuExit = filemenu.Append(wx.ID_EXIT, 'Exit', help='Terminate the program.')

        viewmenu = wx.Menu()
        menuShowProgress = viewmenu.Append(-1, '&Progress...', help='Show progress information.')

        settingsmenu = wx.Menu()
        menuChangeUser = settingsmenu.Append(-1, '&Change User', help='Change the current user.')
        menuConfigureCat = settingsmenu.Append(-1, 'Con&figure Categories',
                                               help='Configure categories. Restart may be required.')
        menuConfigureIni = settingsmenu.Append(-1, 'Configure &Init',
                                               help='Configure textrev.ini file. Restart may be required.')

        helpmenu = wx.Menu()
        menuAbout = helpmenu.Append(wx.ID_ABOUT, '&About...', help='Information about this program.')

        # menubar
        menubar = wx.MenuBar()
        menubar.Append(filemenu, '&File')
        menubar.Append(viewmenu, '&View')
        menubar.Append(settingsmenu, '&Settings')
        menubar.Append(helpmenu, '&Help')
        self.SetMenuBar(menubar)

        self.CreateStatusBar()

        panel = wx.Panel(self)
        self.panel = panel
        # buttons
        self.buttonBox = wx.BoxSizer(wx.HORIZONTAL)
        self.buttonBox_left = wx.BoxSizer(wx.HORIZONTAL)
        self.radio_buttons = []
        #         self.prevButton = wx.Button(panel, -1, "< Prev")
        self.prevButton = TipButton(panel, -1, "< Prev")
        self.prevButton.SetStatusText('Go to previous entry.', self)
        self.prevButton.SetToolTipString('Go to previous entry. If grayed out, there is no previous entry.')
        self.buttonBox_left.Add(self.prevButton, 3, flag=wx.RIGHT)
        self.saveButton = wx.Button(panel, -1, "Save")
        self.prevButton.SetToolTipString('Save progress. This button should be used regularly.')
        self.buttonBox_left.Add(self.saveButton, 3, flag=wx.RIGHT)
        self.nextButton = wx.Button(panel, -1, "Next >")
        self.prevButton.SetToolTipString('Go to next entry. If grayed out, there is no next entry.')
        self.buttonBox_left.Add(self.nextButton, 3, flag=wx.RIGHT)

        self.buttonBox_right = wx.BoxSizer(wx.HORIZONTAL)
        self.comboBox = wx.ComboBox(panel, choices=['Review All', 'Not Annotated Only'], style=wx.CB_READONLY)
        self.comboBox.SetValue('Review All')
        self.buttonBox_right.Add(self.comboBox, 3, flag=wx.ALIGN_RIGHT)

        self.buttonBox.Add(self.buttonBox_left, 3, flag=wx.ALIGN_LEFT)
        self.buttonBox.AddStretchSpacer(1)
        self.buttonBox.Add(self.buttonBox_right, 1, flag=wx.ALIGN_RIGHT)

        self.radio_buttons.append(self.prevButton)
        self.radio_buttons.append(self.saveButton)
        self.radio_buttons.append(self.nextButton)
        self.radio_buttons.append(self.comboBox)

        # layout
        vbox = wx.BoxSizer(wx.VERTICAL)

        vbox.Add((-1, 10))

        hbox0_left = wx.BoxSizer(wx.HORIZONTAL)
        self.patientLabel = wx.StaticText(panel, label='Patient Id: ')
        hbox0_left.Add(self.patientLabel, flag=wx.LEFT, border=8)
        self.patientText = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox0_left.Add(self.patientText, proportion=1)

        hbox0_center = wx.BoxSizer(wx.HORIZONTAL)
        self.currRecordValue = wx.TextCtrl(panel, value='')
        hbox0_center.Add(self.currRecordValue, flag=wx.CENTER, border=8)
        self.currRecordBtn = wx.Button(panel, -1, 'Find')
        hbox0_center.Add(self.currRecordBtn, flag=wx.CENTER, border=8)

        hbox0_right = wx.BoxSizer(wx.HORIZONTAL)
        self.recordLabel = wx.StaticText(panel, label='Record #')
        hbox0_right.Add(self.recordLabel, flag=wx.RIGHT, border=8)
        self.recordText = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox0_right.Add(self.recordText, proportion=1, flag=wx.ALIGN_RIGHT)

        self.recordTotalLabel = wx.StaticText(panel, label=' of ')
        hbox0_right.Add(self.recordTotalLabel, flag=wx.RIGHT, border=8)
        self.recordTotalText = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox0_right.Add(self.recordTotalText, proportion=1, flag=wx.ALIGN_RIGHT)

        #         self.progressBar = wx.Gauge(panel, -1, self.controller.size())

        hbox0 = wx.BoxSizer(wx.HORIZONTAL)
        hbox0.Add(hbox0_left, 5, wx.ALIGN_LEFT, 4)
        hbox0.AddStretchSpacer(1)
        hbox0.Add(hbox0_center, 5, wx.ALIGN_CENTER, 4)
        #         hbox0.AddStretchSpacer(5)
        hbox0.AddStretchSpacer(1)
        #         hbox0.Add(self.progressBar, 5)
        #         hbox0.AddStretchSpacer(1)
        hbox0.Add(hbox0_right, 3, wx.ALIGN_RIGHT, 4)
        vbox.Add(hbox0, border=10, flag=wx.EXPAND)

        vbox.Add((-1, 10))

        hbox1_left = wx.BoxSizer(wx.HORIZONTAL)
        self.conceptLabel = wx.StaticText(panel, label='Concept: ')
        hbox1_left.Add(self.conceptLabel, flag=wx.LEFT, border=8)
        self.conceptText = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox1_left.Add(self.conceptText, proportion=1)

        hbox1_center = wx.BoxSizer(wx.HORIZONTAL)
        self.timeLabel = wx.StaticText(panel, label='Last Save: ')
        hbox1_center.Add(self.timeLabel, flag=wx.CENTER, border=8)
        self.timeValue = wx.StaticText(panel, label=self.controller.get_current_time())
        hbox1_center.Add(self.timeValue, flag=wx.CENTER, border=8)

        hbox1_right = wx.BoxSizer(wx.HORIZONTAL)
        self.userLabel = wx.StaticText(panel, label='User: ')
        hbox1_right.Add(self.userLabel, flag=wx.RIGHT, border=8)
        self.userText = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox1_right.Add(self.userText, proportion=1, flag=wx.ALIGN_RIGHT)

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        hbox1.Add(hbox1_left, 5, wx.ALIGN_LEFT, 4)
        hbox1.AddStretchSpacer(1)
        hbox1.Add(hbox1_center, 5, wx.ALIGN_CENTER, 4)
        hbox1.AddStretchSpacer(1)
        hbox1.Add(hbox1_right, 3, wx.ALIGN_RIGHT, 4)
        vbox.Add(hbox1, border=10, flag=wx.EXPAND)

        vbox.Add((-1, 10))

        # extra headers
        hbox11_left = wx.BoxSizer(wx.HORIZONTAL)
        self.extraLabel1 = wx.StaticText(panel, label='{}: '.format(self.controller.get_extra_header(0)))
        hbox11_left.Add(self.extraLabel1, flag=wx.LEFT, border=8)
        self.extraText1 = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox11_left.Add(self.extraText1, proportion=1)

        hbox11_center = wx.BoxSizer(wx.HORIZONTAL)
        self.extraLabel2 = wx.StaticText(panel, label='{}: '.format(self.controller.get_extra_header(1)))
        hbox11_center.Add(self.extraLabel2, flag=wx.LEFT, border=8)
        self.extraText2 = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox11_center.Add(self.extraText2, proportion=1)

        hbox11_right = wx.BoxSizer(wx.HORIZONTAL)
        self.extraLabel3 = wx.StaticText(panel, label='{}: '.format(self.controller.get_extra_header(2)))
        hbox11_right.Add(self.extraLabel3, flag=wx.RIGHT, border=8)
        self.extraText3 = wx.TextCtrl(panel, style=wx.TE_READONLY)
        hbox11_right.Add(self.extraText3, proportion=1, flag=wx.ALIGN_RIGHT)

        hbox11 = wx.BoxSizer(wx.HORIZONTAL)
        hbox11.Add(hbox11_left, 5, wx.ALIGN_LEFT, 4)
        hbox11.AddStretchSpacer(1)
        hbox11.Add(hbox11_center, 5, wx.ALIGN_CENTER, 4)
        hbox11.AddStretchSpacer(1)
        hbox11.Add(hbox11_right, 3, wx.ALIGN_RIGHT, 4)
        vbox.Add(hbox11, border=10, flag=wx.EXPAND)

        vbox.Add((-1, 10))

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.contextLabel = wx.StaticText(panel, label='Context: ')
        hbox2.Add(self.contextLabel, flag=wx.RIGHT)
        self.contextText = HighlightRichTextCtrl(panel, size=(-1, 50), style=wx.TE_READONLY | wx.TE_MULTILINE)
        hbox2.Add(self.contextText, proportion=1, flag=wx.EXPAND)
        vbox.Add(hbox2, border=10, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP)

        vbox.Add((-1, 10))

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.textLabel = wx.StaticText(panel, label='Text: ')
        hbox3.Add(self.textLabel, flag=wx.LEFT, border=8)
        vbox.Add(hbox3, border=10)

        hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.textText = HighlightRichTextCtrl(panel, style=wx.TE_READONLY | wx.TE_BESTWRAP | wx.TE_MULTILINE)
        hbox4.Add(self.textText, proportion=1, flag=wx.EXPAND)
        vbox.Add(hbox4, proportion=1, flag=wx.LEFT | wx.RIGHT | wx.EXPAND, border=10)

        vbox.Add((-1, 10))

        # radio button flags
        self.grid = wx.GridSizer(rows=0, cols=0)
        self.grid2 = wx.GridSizer(rows=0, cols=0)
        self.radio_buttons = []
        self.max_label_width = 0
        for column, label in self.controller.get_column_label_pairs():
            button_type = self.controller.get_category_button(label)
            width = len(label) * 7
            if width > self.max_label_width:
                self.max_label_width = width
            else:
                width = self.max_label_width
            if button_type == 'Radio':
                for j, lab in enumerate(label.split('=')):
                    if j == 0:
                        button = wx.RadioButton(panel, -1, lab, name=column, size=wx.Size(width, 20), style=wx.RB_GROUP)
                    else:
                        button = wx.RadioButton(panel, -1, lab, name=column, size=wx.Size(width, 20))
                    button.SetValue(False)
                    self.label_to_buttons[column].append(button)
                    self.grid.Add(button, flag=wx.EXPAND)
                    self.radio_buttons.append(button)
            elif button_type == 'Text':
                hbox = wx.BoxSizer(wx.HORIZONTAL)
                hbox.Add(wx.StaticText(panel, label=label))
                btn = wx.TextCtrl(panel, name=column)
                btn.Bind(wx.EVT_KEY_UP, self.OnKeyUpText)
                self.label_to_buttons[column].append(btn)
                hbox.Add(btn)
                self.grid.Add(hbox)
        done_name = self.controller.completed_column_name()
        done_button = wx.RadioButton(panel, -1, 'Done', name=done_name, style=wx.RB_GROUP)
        not_done_button = wx.RadioButton(panel, -1, 'Not Done', name=done_name)
        self.label_to_buttons[done_name].append(done_button)
        self.label_to_buttons[done_name].append(not_done_button)
        self.grid2.Add(done_button)
        self.grid2.Add(not_done_button)
        horizRadioBox = wx.BoxSizer(wx.HORIZONTAL)
        horizRadioBox.Add(self.grid, flag=wx.EXPAND, border=10)
        horizRadioBox.AddStretchSpacer(1)
        horizRadioBox.Add(self.grid2, flag=wx.EXPAND, border=10)
        vbox.Add(horizRadioBox, border=10)

        vbox.Add((-1, 10))

        # button box
        vbox.Add(self.buttonBox, flag=wx.EXPAND, border=10)
        panel.SetSizer(vbox)

        # events
        self.Bind(wx.EVT_MENU, self.OnAbout, menuAbout)
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)
        self.Bind(wx.EVT_MENU, self.OnSaveAndExit, menuSaveExit)
        self.Bind(wx.EVT_MENU, self.OnExportFile, exportFile)
        self.Bind(wx.EVT_MENU, self.OnConfigureInit, menuConfigureIni)
        self.Bind(wx.EVT_MENU, self.OnConfigureCategories, menuConfigureCat)
        self.Bind(wx.EVT_CLOSE, self.OnExit)
        self.Bind(wx.EVT_QUERY_END_SESSION, self.OnExit)
        self.Bind(wx.EVT_END_SESSION, self.OnExit)
        self.Bind(wx.EVT_MENU, self.GetUser, menuChangeUser)
        self.Bind(wx.EVT_MENU, self.OnSave, menuSave)
        self.Bind(wx.EVT_MENU, self.OnShowProgress, menuShowProgress)
        self.Bind(wx.EVT_SIZE, self.OnFrameResize)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnRadioButtonChecked)
        self.Bind(wx.EVT_BUTTON, self.OnPrev, self.prevButton)
        self.Bind(wx.EVT_BUTTON, self.OnNext, self.nextButton)
        self.Bind(wx.EVT_BUTTON, self.OnSave, self.saveButton)
        self.Bind(wx.EVT_BUTTON, self.OnDoneButton, done_button)
        self.Bind(wx.EVT_BUTTON, self.OnNotDoneButton, not_done_button)
        self.Bind(wx.EVT_COMBOBOX, self.OnComboBoxChange, self.comboBox)
        self.Bind(wx.EVT_BUTTON, self.OnRecordButton, self.currRecordBtn)

    def OnKeyUpText(self, ev):
        self.controller.label_selected(ev.EventObject.Name, ev.EventObject.GetValue())

    def UpdateButtonLayout(self):
        panelwidth = self.panel.GetSize().width
        factor = float(panelwidth) / self.max_label_width
        self.grid.SetCols(math.ceil(float(factor) / 2))
        self.grid2.SetCols(math.ceil(float(factor) / 2))
        nr, _ = self.grid.CalcRowsCols()
        self.grid.SetRows(nr)
        self.grid2.SetRows(nr)
        self.panel.Layout()

    def OnFrameResize(self, event):
        self.UpdateButtonLayout()
        event.Skip()

    def OnAbout(self, ev):
        dlg = wx.MessageDialog(self, 'TextReviewer, version 0\n\nCreated by David Cronkite, GHRI', 'About', style=wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

    def OnSaveAndExit(self, ev):
        self.OnSave(ev)
        self.Destroy()

    def OnRecordButton(self, ev):
        self.controller.set_current_record_number(self.currRecordValue.GetValue())
        self.EnableButtons()
        self.PopulateFields()

    def OnConfigureInit(self, ev):
        dlg = ConfigureInit(self, [])
        if dlg.ShowModal() == wx.OK:
            dlg.getUpdatedConfigs()

    def OnConfigureCategories(self, ev):
        dlg = ConfigureCategoriesDialog(self, self.controller.get_all())
        if dlg.ShowModal() == wx.ID_OK:
            self.controller.update_categories(dlg.getCategories())
            self.ResetRadioButtons()
            self.grid.Layout()
            self.grid2.Layout()
        self.UpdateButtonLayout()

    def ResetRadioButtons(self):
        self.RemoveRadioButtons()
        self.AddRadioButtons()
        self.UpdateButtonLayout()

    def AddRadioButtons(self):
        self.radio_buttons = []
        self.max_label_width = 0
        for num, label in enumerate(self.controller.get_categories()):
            width = len(label) * 6
            value = self.controller.get_category_button(label)
            if width > self.max_label_width:
                self.max_label_width = width
            if num == 0:
                button = wx.RadioButton(self.panel, -1, label, size=wx.Size(width, 20), style=wx.RB_GROUP)
            else:
                button = wx.RadioButton(self.panel, -1, label, size=wx.Size(width, 20))
            button.SetValue(False)
            if value == 1:
                self.grid.Add(button, flag=wx.EXPAND)
            else:
                self.grid2.Add(button, proportion=1)
            self.radio_buttons.append(button)
        self.grid.Add(self.new_radio_button)

    def OnExit(self, ev):
        dlg = wx.MessageDialog(self,
                               'Do you want to save before exiting?\nIf you choose "No", all progress will be lost.',
                               'Save Progress Before Exit?',
                               wx.YES | wx.NO | wx.CANCEL | wx.ICON_EXCLAMATION)
        res = dlg.ShowModal()
        if res == wx.ID_YES:
            self.OnSave(ev)
            self.Destroy()
        elif res == wx.ID_NO:
            self.Destroy()

    def OnShowProgress(self, ev):
        total = self.controller.size()
        completed = self.controller.get_completed()
        percent = 100 * (float(completed) / total)
        dlg = wx.ProgressDialog('Progress', '', total, self, style=wx.PD_CAN_ABORT)
        while True:
            res = dlg.Update(completed, 'Completed: %.1f%% (%d/%d).' % (percent, completed, total))
            if res[0]:
                continue
            else:
                dlg.Destroy()
                break

    def OnRadioButtonChecked(self, ev):
        self.controller.label_selected(ev.EventObject.Name, ev.EventObject.Label)

    def OnDoneButton(self, ev):
        self.controller.mark_done(True)

    def OnNotDoneButton(self, ev):
        self.controller.mark_done(False)

    def OnPrev(self, ev):
        self.controller.save()
        self.controller.prev()
        self.EnableButtons()
        self.PopulateFields()

    def OnNext(self, ev):
        self.controller.save()
        self.controller.next()
        self.EnableButtons()
        self.PopulateFields()

    def EnableButtons(self):
        if self.controller.has_next():
            self.nextButton.Enable()
        else:
            self.nextButton.Disable()
        if self.controller.has_prev():
            self.prevButton.Enable()
        else:
            self.prevButton.Disable()

    def OnSave(self, ev):
        self.controller.save(force=True)
        self.timeValue.SetLabel(self.controller.get_current_time())

    def OnComboBoxChange(self, ev):
        if self.comboBox.GetValue() == 'Review All':
            self.controller.review_mode = True
        elif self.comboBox.GetValue() == 'Not Annotated Only':
            self.controller.review_mode = False
        self.EnableButtons()

    def OnNewRadioButton(self, ev):
        dlg = TwoTextEntryDialog(self.panel, [])
        if dlg.ShowModal() == wx.ID_OK:
            label = dlg.getDescription()
            self.grid.Remove(self.new_radio_button)
            width = len(label) * 6
            if width > self.max_label_width:
                self.max_label_width = width
            button = wx.RadioButton(self.panel, -1, label, size=wx.Size(width, 20))
            self.grid.Add(button, flag=wx.EXPAND)
            self.grid.Add(self.new_radio_button)
            self.radio_buttons.append(button)
            self.controller.add_category(label)
            self.UpdateButtonLayout()

    def RemoveRadioButtons(self):
        print('Remove')
        self.grid.Remove(self.new_radio_button)
        self.grid.Detach(self.new_radio_button)
        for button in self.radio_buttons:
            self.grid.Remove(button)
            self.grid.Detach(button)
            self.grid.Hide(button)
            button.Destroy()

        self.grid.DeleteWindows()
        self.grid.Layout()

    def PopulateFields(self):
        self.textText.SetValueAndHighlight(self.controller.get_text(), self.controller.get_concept())
        self.contextText.SetValueAndHighlight(self.controller.get_context(), self.controller.get_concept())
        self.conceptText.SetValue(self.controller.get_concept())
        self.patientText.SetValue(self.controller.get_patient_id())
        self.recordText.SetValue(self.controller.get_record_number())
        self.recordTotalText.SetValue(self.controller.get_record_total())
        self.extraText1.SetValue(self.controller.get_extra_text(0))
        self.extraText2.SetValue(self.controller.get_extra_text(1))
        self.extraText3.SetValue(self.controller.get_extra_text(2))
        labels = self.controller.get_labels()
        for label in labels:
            value = labels[label]
            buttons = self.label_to_buttons[label]
            if isinstance(buttons[0], wx.RadioButton):
                for btn in buttons:
                    if value and btn.GetLabel() == value:
                        btn.SetValue(True)
                    else:
                        btn.SetValue(False)
            else:  # text
                buttons[0].SetValue(value)
        self.currRecordValue.SetValue(self.controller.get_current_record_number())

    def GetUser(self, ev=None):
        dlg = wx.TextEntryDialog(self, 'User: ', 'Enter Username')
        user = ''
        while True:
            if dlg.ShowModal() == wx.ID_OK:
                user = dlg.GetValue().strip()
                self.userText.SetValue(user)
                self.controller.change_user(user)
            if len(user) > 0:
                break
        self.PopulateFields()
        self.EnableButtons()

    def OnExportFile(self, ev):
        self.controller.export_file()
