"""
filename.py

"""
import argparse
import logging.config

import sys
import wx

from gui import MainWindow
from util import mylogger

if __name__ == '__main__':
    parser = argparse.ArgumentParser(fromfile_prefix_chars="@")
    parser.add_argument('--categories', nargs='+')
    parser.add_argument('--sourcefile')
    parser.add_argument('--datadirectory')
    parser.add_argument('--concept')
    parser.add_argument('--context')
    parser.add_argument('--text', nargs='+')

    parser.add_argument('--patient-id')
    parser.add_argument('--record-number')
    parser.add_argument('--record-total')
    parser.add_argument('--extra-headers', nargs='+',
                        help='List of other headers (max of 3) to include.')

    try:
        print(sys.argv)
        if len(sys.argv) > 1:
            args = parser.parse_args()
        else:
            args = parser.parse_args([r'@.\textrev.ini'])
    except:
        logging.exception('Failed to parse starting configurations.')
        logging.error('Problem with ini file.')
        raise ValueError('Unable to find textrev.ini file.')

    loglevel = logging.DEBUG
    mylogger.setup(name='textrev', loglevel=loglevel)

    logging.info('Starting application.')

    app = wx.App(False)

    try:
        frame = MainWindow(None, 'Text Reviewer', **vars(args))
        app.MainLoop()
    except Exception as e:
        logging.exception(e)
        logging.error('Process terminated with errors.')
    else:
        logging.info('Process completed.')
