"""
Author: David Cronkite, GHRI
Date: 20dec12
Purpose:
    Methods for simplying the creation of a logfile
"""

import logging
from logging.handlers import RotatingFileHandler
import os

from unix import mkdir_p


def setup(name=__name__, logdir='.', console=True, text=True, loglevel=logging.DEBUG, logfile=None):
    """
    function:
        prepares a logger that can print to both the console and a logfile
    parameters:
        name - the logger's name (for the call logging.getLogger(name))
        logdir - location for the output logfile (default is current dir)
        console - print to console
        text - print to logfile
        loglevel - level for logger (logging.DEBUG)
        logfile - name of logfile (default=name + '.log')
    use:
        mylogger.setup(name, etc.)
        logger = mylogger.getLogger(name)
    """
    logdir = os.path.abspath(logdir)
    mkdir_p(logdir)
    logger = logging.getLogger(name)
    logger.setLevel(loglevel)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s')

    if not logfile:
        logfile = name + '.log'

    if text:
        txt_handler = RotatingFileHandler(os.path.join(logdir, logfile), backupCount=5)
        txt_handler.doRollover()
        txt_handler.setFormatter(formatter)
        logger.addHandler(txt_handler)

    if console:
        scrn_handler = logging.StreamHandler()
        scrn_handler.setFormatter(formatter)
        logger.addHandler(scrn_handler)

    return logging.getLogger(name)


def get_logger(name=__name__):
    return logging.getLogger(name)
