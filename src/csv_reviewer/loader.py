"""
Loads data and returns (header, data)
"""
import cPickle as pickle
import csv
import os

from util.unix import mkdir_p


def load_from_file(fn, delimiter='\t'):
    if fn[-3:].lower() == 'csv':
        return load_from_csv(fn)
    elif fn[-3:].lower() == 'pkl':
        return load_from_pickle(fn)
    else:
        return load_from_text_file(fn)


def load_from_text_file(fn, delimiter='\t'):
    header = None
    data = []
    with open(fn) as f:
        for num, line in enumerate(f.readlines()):
            line = line.strip()
            if not line:
                continue
            if num == 0:  # header
                header = line.split(delimiter)
            else:
                data.append(line.split(delimiter))
    return header, data


def load_from_csv(fn):
    header = None
    data = []
    with open(fn, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for num, row in enumerate(reader):
            if num == 0:  # header
                header = row
            else:
                data.append(row)
    return header, data


def load_from_pickle(fn):
    with open(fn, 'rb') as input:
        return pickle.load(input)


def get_most_recent_file(datadir):
    datapath = os.path.abspath(datadir)
    if os.path.exists(datapath):
        try:
            fn = sorted(os.listdir(datapath), reverse=True)[0]
        except IndexError:
            return None
        return os.path.join(datapath, fn)
    else:
        mkdir_p(datapath)
        return None


def convert_results_to_csv(idir, ofn):
    res = {}
    lst = []
    header = []
    full_header = ['Record']
    for nimi in os.listdir(idir):
        tp = os.path.join(idir, nimi)
        lst = load_from_pickle(get_most_recent_file(tp))
        if not header:
            header = list(lst[0].keys())
        full_header += ['{}-{}'.format(nimi, h) for h in header]
        res[nimi] = lst
    with open(ofn, 'wb') as out:
        writer = csv.writer(out)
        writer.writerow(full_header)
        for i in range(len(lst)):
            rivi = [i]
            for nimi in res:
                for head in header:
                    rivi.append(res[nimi][i][head])
            writer.writerow(rivi)
