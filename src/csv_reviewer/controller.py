"""
Controller class.

"""

from loader import load_from_file, convert_results_to_csv
from data import ConceptCask, Categories
from util.unix import mkdir_p

import cPickle as pickle
import datetime
import logging
import os


class Controller(object):
    def __init__(self, **configs):
        self.review_mode = True
        self.change_detected = False
        self.current_user_ = None
        eqdict = {'text': configs.get('text', None),
                  'context': configs.get('context', None),
                  'concept': configs.get('concept', None),
                  'patient-id': configs.get('patient_id', None),
                  'record-number': configs.get('record_number', None),
                  'record-total': configs.get('record_total', None),
                  'extra-headers': configs.get('extra_headers', list())
                  }
        self.data_dir_ = configs['datadirectory']
        mkdir_p(self.data_dir_)
        fn = os.path.abspath(configs['sourcefile'])
        logging.info('Loading starting file: "%s".' % fn)
        header, data = load_from_file(fn)
        self.data_ = ConceptCask(header, data, eqdict)
        self.categories = Categories([el.strip().split('==') for el in configs['categories']])

    @staticmethod
    def get_most_recent_file(datadir):
        datapath = os.path.abspath(datadir)
        if os.path.exists(datapath):
            try:
                fn = sorted(os.listdir(datapath), reverse=True)[0]
            except IndexError:
                return None
            return os.path.join(datapath, fn)
        else:
            mkdir_p(datapath)
            return None

    def get_category_button(self, cat):
        return self.categories.get_category_button(cat)

    def save(self, force=False):
        if not force and not self.change_detected:
            logging.warning('Skipping save: No change detected.')
            return
        temp = os.path.join(self.data_dir_, self.current_user_, '0.fail')
        final = os.path.join(self.data_dir_, self.current_user_, self.get_current_time(False) + '.pkl')
        with open(temp, 'wb') as out:
            pickle.dump(self.data_.userdata_, out, protocol=-1)
        try:
            os.rename(temp, final)
        except WindowsError as we:
            logging.warning('Multiple saves in same second. Ignoring.')
            logging.exception(we)
        else:
            self.change_detected = False
            logging.info('Wrote file.')

    def has_next(self):
        if self.review_mode:
            return self.data_.has_next()
        else:  # annotation mode
            return self.data_.has_next_unanswered() != -1

    def next(self):
        if self.review_mode:
            return self.data_.next()
        else:
            return self.data_.next_unanswered()

    def has_prev(self):
        if self.review_mode:
            return self.data_.has_prev()
        else:  # annotation mode
            return self.data_.has_prev_unanswered() != -1

    def prev(self):
        if self.review_mode:
            return self.data_.prev()
        else:
            self.data_.prev_unanswered()

    def get_categories(self):
        return self.categories.get_categories()

    def get_all(self):
        return self.categories.get_all()

    def update_categories(self, lst):
        self.categories.update_categories(lst)

    def add_category(self, cat):
        self.categories.add_category(cat)

    @staticmethod
    def get_current_time(normal=True):
        if normal:
            return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S%z')
        else:  # for filename
            return datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S%z')

    def label_selected(self, col_name, label):
        self.change_detected = True
        self.data_.add_input(col_name, label)

    def get_text(self):
        return self.data_.get_text()

    def get_context(self):
        return self.data_.get_context()

    def get_concept(self):
        return self.data_.get_concept()

    def get_patient_id(self):
        return self.data_.get_patient_id()

    def get_record_number(self):
        return self.data_.get_record_number()

    def get_record_total(self):
        return self.data_.get_record_total()

    def get_labels(self):
        return self.data_.get_current_row()

    def change_user(self, user):
        dn = os.path.join(self.data_dir_, user)
        fn = self.get_most_recent_file(dn)
        if fn and fn[-3:] == 'pkl':
            userdata = load_from_file(fn)
        else:
            userdata = [{column: '' for column in self.categories.columns()} for _ in range(self.size())]
        self.data_.change_user(userdata)
        self.current_user_ = user
        logging.info('Changed user to "%s".' % user)

    def size(self):
        return len(self.data_)

    def get_completed(self):
        return self.get_progress()[0]

    def get_progress(self):
        return self.data_.progress()

    def get_current_record_number(self):
        return str(self.data_.get_row())

    def set_current_record_number(self, num):
        try:
            num = int(num)
        except ValueError as ve:
            logging.error(ve)
            logging.exception('"%s" is not a number.' % num)
            return False
        if num < 0:
            num = 0
        elif num >= len(self.data_):
            num = len(self.data_) - 1
        self.data_.set_row(int(num))

    def mark_done(self, completed=True):
        self.data_.mark_done(completed)

    def completed_column_name(self):
        return self.data_.COMPLETED_COLUMN

    def get_column_label_pairs(self):
        return self.categories.get_column_label_pairs()

    def export_file(self):
        convert_results_to_csv(
            idir=self.data_dir_,
            ofn='export_{}.csv'.format(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S%z'))
        )

    def get_extra_text(self, idx):
        return self.data_.get_extra_text(idx)

    def get_extra_header(self, idx):
        return self.data_.get_extra_header(idx)
