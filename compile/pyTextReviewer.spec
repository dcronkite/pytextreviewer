# -*- mode: python -*-
a = Analysis([os.path.abspath('..\\src\\main.py')],
             pathex=[os.path.abspath('.')],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='pyTextReviewer.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False , icon='textrev.ico')
